# Biblioteca Pessoal

Sistema para registro de livros pessoais.

### Ruby and Rails version 
- Ruby `3.0.0p0`  
- Rails `7.0.2.3`
- Bundler

### Dependências

Docker e docker-compose

### Configuração do Projeto

Toda a configuração customizável pode ser alterada no arquivo .env.
Uma versão desse .env está disponível na raiz do repositório, assim
como os demais arquivos de configuração.

```
docker-compose build
```

### Como rodar a aplicação localmente

Para executar a aplicação com o servidor web embutido basta rodar o comando
abaixo que a mesma será inicializada no localhost na porta 3000. O Projeto
também possui um proxy que permite acessar no localhost na porta 80.

```
docker-compose up # subir aplicação
docker-compose down # pausar aplicação
```

### Como rodar testes localmente 

```
docker-compose up --detach
docker exec rails rspec
docker-compose down
```

### Como rodar lint localmente

```
docker-compose up --detach
docker exec -it rails /bin/bash
gem install rubocop
rubocop
exit
docker-compose down
```

## Pipeline de Integração Contínua (CI)

A pipeline roda rubocop para fazer lint e rspec para executar os testes.
Caso algum desses falhe, os próximos jobs não são executados. Exemplo
de jobs executados:

```
lint: https://gitlab.com/sergiosacj/trabalho-individual-2021-2/-/jobs/2402116118
test: https://gitlab.com/sergiosacj/trabalho-individual-2021-2/-/jobs/2402116119
build_and_publish_image: https://gitlab.com/sergiosacj/trabalho-individual-2021-2/-/commit/ba0011f90d12baa3d6c18cef3b5a5827583f35d8
```

Caso todos passem, a próxima etapa é de build. Nessa etapa a imagem é
construída com uma tag e armazenada no próprio registry do GitLab. Para
usar uma tag publicada, basta indicar da seguinte forma:

```
(Dockerfile)
FROM registry.gitlab.com/sergiosacj/trabalho-individual-2021-2/biblioteca_pessoal:v0.0.0-main.3
(docker-compose)
image: registry.gitlab.com/sergiosacj/trabalho-individual-2021-2/biblioteca_pessoal:v0.0.0-main.3
```

Além disso, é importante observar que a etapa de "build_and_publish_image" só ocorre
quando se cria tag. É entendido que a criação de tags caracteriza uma release, assim,
só será gerado imagens e feito deploy nesse cenário.
